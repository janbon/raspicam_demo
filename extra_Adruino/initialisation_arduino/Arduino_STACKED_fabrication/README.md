What its all about
------------------

This doc explain how to craft your own microscope controler.

Get more info with the following tutorials:
[Adafruit motor shield V2](https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino/overview)
[Stacking shields](https://learn.adafruit.com/adafruit-motor-shield-v2-for-arduino/stacking-shields)


Material
--------


- an arduino card [LINK](https://store.arduino.cc/arduino-uno-rev3)

- 2 adafruit motor shield V2 [LINK](https://www.adafruit.com/product/1438)

- soldering material, some jumper wire M/M [LINK](https://www.reichelt.com/fr/en/flexible-jumper-wire-set-5cm-male-male-steckboard-kssrt-p177334.html?&trstct=pos_8)

- some domino [LINK](https://www.reichelt.com/fr/fr/domino-d-vissable-2-5-mm-noir-cb-lksw-2-5-p133930.html?&trstct=pos_3)

- some stackable headers [LINK](https://www.adafruit.com/product/2830)


- power unit. Supplying at least 12V and 1.5A for each motor via the shield. 3.3V for the Arduino ( :volcano: it need to be powered from another source than the Serial). In our case we used an old computer alimentation block. That we saffely operated, be carrefull its 220V inside.


Fabrication
-----------

First of all you need to solder the stackable pins onto the shields. Such as following.  
![Alt text](soldering_pin.png?raw=true "how to solder the stackable pin")


Then you need to solder the adress jumper on the shields, in order to generate there adresses.  

Board 0: Address = 0x60 Offset = binary 0000 (no jumpers required)  
Board 1: Address = 0x61 Offset = binary 0001 (bridge A0 as in the photo above)  
Board 2: Address = 0x62 Offset = binary 0010 (bridge A1, to the left of A0)  
Board 3: Address = 0x63 Offset = binary 0011 (bridge A0 & A1, two rightmost jumpers)  
Board 4: Address = 0x64 Offset = binary 0100 (bridge A2, middle jumper)  
```C
Adafruit_MotorShield AFMSbot(0x61); // Rightmost jumper closed
Adafruit_MotorShield AFMStop(0x60); // Default address, no jumpers
```
Be carefull to not heat to much during the soldering!  
![Alt text](adafruit_adress.png?raw=true "fix the adress by soldering the first adress jumper")


In order to acces the lower shield, fix jumper wire into the different connectors. 
![Alt text](photo_domino.png?raw=true "use a dongle system to access the lower shield.")


Put in place the control LED, between the ground of the arduino (you need to multiplex one of the Ground) and the analog pin 0, 1 and 2.
![Alt text](photo_controlLED.png?raw=true "use some control LED to helps you debug the system if needed.")


:volcano: Put in place the Ground wire that will be connected to the ground of the serial connected device that will control the Arduino.


Motors connection
-----------------

The motors need to be connected on the shield. 
This explaination works for 4 wires Bipolar? motors.

Bottom shield
M1 Gnd Gnd M2

Top shield
M1 Gnd Gnd M2

M3 Gnd Gnd M4