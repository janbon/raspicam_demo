
//Adafruit Functions
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
//#include <Adafruit_MS_PWMServoDriver.h>



//adress the bottom shield
Adafruit_MotorShield AFMSbot = Adafruit_MotorShield(0x60);
//adress the top shield
Adafruit_MotorShield AFMStop = Adafruit_MotorShield(0x61);



//Get the Xmotor from the bottom shield
//Adafruit_StepperMotor *stepperX = AFMSbot.getStepper(200, 1);
//Get the fan from the bottom shield
Adafruit_DCMotor *myFAN1 = AFMSbot.getMotor(2);
//Get the Ymotor from the top shield
//Adafruit_StepperMotor *stepperY = AFMStop.getStepper(200, 1);
//Get the Zmotor from the top shield
//Adafruit_StepperMotor *stepperZ = AFMStop.getStepper(200, 2);
Adafruit_DCMotor *myFAN2 = AFMStop.getMotor(3);


//define constants
String val;
String motor;
String amplitude;


int ledX = 9; // the pin the bottom X LED is connected to

int ledY = 10; // the pin the top Y LED is connected to

int ledZ = 2; // the pin the top Z LED is connected to

void setup()
{
  pinMode(ledX, OUTPUT); // Declare the LED as an output
  pinMode(ledY, OUTPUT); // Declare the LED as an output
  pinMode(ledZ, OUTPUT); // Declare the LED as an output
  
  Serial.begin(9600);
  
  AFMStop.begin();  // initialize top shield
  AFMSbot.begin();  // initialize bottom shield

  // Set the speed for the differents motors  
  //stepperX->setSpeed(250);  // rpm   
  //stepperY->setSpeed(250);  // rpm 
  //stepperZ->setSpeed(250);  // rpm   

  // Blink the LED
  digitalWrite(ledX, HIGH); // Turn the LED on
  digitalWrite(ledY, HIGH); // Turn the LED on
  digitalWrite(ledZ, HIGH); // Turn the LED on
  delay(1000);
  digitalWrite(ledX, LOW); // Turn the LED off
  digitalWrite(ledY, LOW); // Turn the LED off
  digitalWrite(ledZ, LOW); // Turn the LED off 

  // Test the Fan
  myFAN1->setSpeed(20);  
  myFAN1->run(FORWARD);
  delay(1000);
  myFAN1->run(RELEASE);
  // Test the Fan
  myFAN2->setSpeed(20);  
  myFAN2->run(FORWARD);
  delay(1000);
  myFAN2->run(RELEASE);
}





void loop()
{
  delay(10);
    
  if (Serial.available()>0){
    
    val = Serial.readStringUntil('#') ;
     
    if (val == 'X'){
      digitalWrite(ledX, HIGH); // Turn the LED on
      amplitude = Serial.readStringUntil('~');
      int int_amplitude = amplitude.toInt();
      //stepperY->step(1,FORWARD,SINGLE);
      Serial.println("F"); 
      digitalWrite(ledX, LOW); // Turn the LED off     
      }
    
    if (val == 'Y'){
      //stepperY->step(1,BACKWARD,SINGLE);
      Serial.println("F"); 
      }
    
    if (val == 'Z'){
      //stepperX->step(1,FORWARD,SINGLE);
      Serial.println("F"); 
      }
    

  }
  
}
