
//Adafruit Functions
#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_MS_PWMServoDriver.h"
//#include <Adafruit_MS_PWMServoDriver.h>



//adress the bottom shield
Adafruit_MotorShield AFMSbot = Adafruit_MotorShield(0x60);
//adress the top shield
Adafruit_MotorShield AFMStop = Adafruit_MotorShield(0x61);



//Get the Xmotor from the bottom shield
Adafruit_StepperMotor *stepperX = AFMSbot.getStepper(200, 1);
//Get the fan from the bottom shield
Adafruit_DCMotor *myFAN1 = AFMSbot.getMotor(3);
//Get the Ymotor from the top shield
Adafruit_StepperMotor *stepperY = AFMStop.getStepper(200, 1);
//Get the Zmotor from the top shield
Adafruit_StepperMotor *stepperZ = AFMStop.getStepper(200, 2);

//define constants
String val;
String motor;
String amplitude;

int ledSERIAL = A4 // the pin the Serial LED is connected to
int ledX = A0; // the pin the bottom X LED is connected to
int ledY = A1; // the pin the top Y LED is connected to
int ledZ = A2; // the pin the top Z LED is connected to

void setup()
{
  pinMode(ledX, OUTPUT); // Declare the LED as an output
  pinMode(ledY, OUTPUT); // Declare the LED as an output
  pinMode(ledZ, OUTPUT); // Declare the LED as an output
  
  Serial.begin(9600);
  
  AFMStop.begin();  // initialize top shield
  AFMSbot.begin();  // initialize bottom shield

  // Set the speed for the differents motors  
  //stepperX->setSpeed(250);  // rpm   
  stepperY->setSpeed(250);  // rpm 
  stepperZ->setSpeed(250);  // rpm   

  // Blink the LED
  digitalWrite(ledX, HIGH); // Turn the LED on
  digitalWrite(ledY, HIGH); // Turn the LED on
  digitalWrite(ledZ, HIGH); // Turn the LED on
  delay(1000);
  digitalWrite(ledX, LOW); // Turn the LED off
  digitalWrite(ledY, LOW); // Turn the LED off
  digitalWrite(ledZ, LOW); // Turn the LED off 

  // Test the Fan
  myFAN1->setSpeed(20);  
  myFAN1->run(FORWARD);
  delay(1000);
  myFAN1->run(RELEASE);
}





void loop()
{
  delay(10);
    
  if (Serial.available()>0){
    digitalWrite(ledSERIAL, HIGH); // Turn the LED on
    val = Serial.readStringUntil('#') ;

    if (val == 'X'){
      digitalWrite(ledX, HIGH); // Turn the LED on
      amplitude = Serial.readStringUntil('~');
      int int_amplitude = amplitude.toInt();
      if (val > 0){
        stepperX->step(val,FORWARD,SINGLE);
        //CAUTION SUPER IMPORTANT TO RELEASE STEPPER, ELSE CAN HEAT UP!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        stepperX->release();
        }       
      if (val < 0){
        stepperX->step(-val,BACKWARD,SINGLE);
        //CAUTION SUPER IMPORTANT TO RELEASE STEPPER, ELSE CAN HEAT UP!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        stepperX->release();
        }
      Serial.println("F"); 
      digitalWrite(ledX, LOW); // Turn the LED off     
      }
    
    if (val == 'Y'){
      digitalWrite(ledY, HIGH); // Turn the LED on
      amplitude = Serial.readStringUntil('~');
      int int_amplitude = amplitude.toInt();
      if (val > 0){
        stepperY->step(val,FORWARD,SINGLE);
        //CAUTION SUPER IMPORTANT TO RELEASE STEPPER, ELSE CAN HEAT UP!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        stepperY->release();
        }       
      if (val < 0){
        stepperY->step(-val,BACKWARD,SINGLE);
        //CAUTION SUPER IMPORTANT TO RELEASE STEPPER, ELSE CAN HEAT UP!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        stepperY->release();
        }
      Serial.println("F"); 
      digitalWrite(ledY, LOW); // Turn the LED off   
      }
    
    if (val == 'Z'){
      digitalWrite(ledZ, HIGH); // Turn the LED on
      amplitude = Serial.readStringUntil('~');
      int int_amplitude = amplitude.toInt();
      if (val > 0){
        stepperZ->step(val,FORWARD,SINGLE);
        //CAUTION SUPER IMPORTANT TO RELEASE STEPPER, ELSE CAN HEAT UP!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        stepperZ->release();
        }       
      if (val < 0){
        stepperZ->step(-val,BACKWARD,SINGLE);
        //CAUTION SUPER IMPORTANT TO RELEASE STEPPER, ELSE CAN HEAT UP!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        stepperZ->release();
        }
      Serial.println("F"); 
      digitalWrite(ledZ, LOW); // Turn the LED off  
      }
      digitalWrite(ledSERIAL, LOW); // Turn the LED on

  }
  
}
