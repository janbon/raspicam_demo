"""Summary

Attributes:
    camera (TYPE): Description
    end (bool): Description
    fake_camera (bool): Description
    showme (bool): Description
"""
# encoding: utf-8

"""This is the a camera example. It will ... let see without AMQP

.. moduleauthor:: Charles Fosseprez

.. contact:: charles.fosseprez@cri-paris.org

"""

###
# Import generics
import time
from time import sleep
import multiprocessing
import json
###

###
# Import camera
fake_camera = False  # to tune the import
if fake_camera is False:
    import picamera
elif fake_camera is True:
    import magik_camera.picamera_fake as picamera
###



###
# Should I display?
global showme
showme = True  # the showme parameters allow to tune the display of text
print("[i] display camera_process is set to: " + showme)


def checkit_(iamtext):
    """Summary
    
    Args:
        iamtext (TYPE): Description
    """
    global showme
    if showme is True:
        print(iamtext)
###


###############################################################################
#
#
#             """Principal processs"""
#
#
###############################################################################
###############################################################################
"""this process control the screen via command_queue"""
###############################################################################


def camera_process(command_queue):
    ###############################################################################
    """this maintain the main process alive"""
    ###############################################################################

    ###
    # Catch camera
    camera = picamera.PiCamera()   # get the camera
    ###
    # Capture setting
    camera.awb_gains = (1.8, 1.5)
    camera.shutter_speed = 30000
    camera.exposure_mode = 'off'

    ###
    # Capture proper
    sleep(2)  # Wait for it to initiialise



    end = False
    while not end:
        while not command_queue.empty():
            command = command_queue.get()
            action_broker(command, camera)


###############################################################################
"""this is the action_broker"""
###############################################################################


def action_broker(command, camera):
    """Summary
    
    Args:
        command (TYPE): Description
        camera (TYPE): Description
    
    Deleted Parameters:
        surf (TYPE): Description
    """
    global showme

    trans_cmd = command
    command = json.loads(trans_cmd)
    order = command['order']

    checkit_('------------------')
    checkit_("[i] New action is: " + order)
    checkit_('------------------')

    if order == 'crash':
        print('Projection crash !!!')
        blitzkrieg('cake', (0,0))
    elif order == 'humpa':
        print('lumpa')
    elif order == 'test':
        camera.capture('test.png', format='png')   # save image in current folder
    elif order == 'setings1':
        checkit_('[>] setings1')
        ###
        # Capture setting
        camera.awb_gains = (1.8, 1.5)
        camera.shutter_speed = 30000
        camera.exposure_mode = 'off'
        sleep(2)  # Wait for it to initiialise
    elif order == 'setings2':
        checkit_('[>] setings2')
        ###
        # Capture setting
        camera.exposure_mode = 'on'
        sleep(2)  # Wait for it to initiialise
    #################################################
    #       """UnKnown"""
    #################################################
    else:
        print('-------------------------------')
        print("Order is unknown 404")


###############################################################################
#
#
#             """Push command"""
#
#
###############################################################################
###############################################################################
"""This allow to pass a json command"""
###############################################################################


def command_pusher(command_queue, order):
    command = {'order': order}
    command_jsoned = json.dumps(command)
    command_queue.put(command_jsoned)


###############################################################################
"""this timer allow to sart a command after a given time"""
###############################################################################


def timer_pusher(command_queue, time_before, command):
    """Summary
    
    Args:
        command_queue (TYPE): Description
        time_before (TYPE): Description
        command (TYPE): Description
    """
    time.sleep(time_before)
    print('[i] timer done, give command: ' + str(command['order']))
    command_jsoned = json.dumps(command)
    command_queue.put(command_jsoned)


###############################################################################
#
#
#             """Process coupler"""
#
#
###############################################################################
###############################################################################
"""this allow to run the process(pygame,rabbit...) asynchronously"""
###############################################################################


def processator():
    """Summary
    """
    #Start the process which will attempt to quit the display
    # https://stackoverflow.com/questions/11515944/how-to-use-multiprocessing-queue-in-python
    # https://www.journaldev.com/15631/python-multiprocessing-example
    # https://pymotw.com/2/multiprocessing/communication.html
    command_queue = multiprocessing.Queue()  

    launch_process = multiprocessing.Process(target = camera_process, args = (command_queue, ))
    launch_process.start()


    time_before = 5
    command = {'order': 'test',}
    timer = multiprocessing.Process(target = timer_pusher, args = (command_queue, time_before, command))
    timer.start()

    
    time_before = 5
    command = {'order': 'humpa',}
    timer = multiprocessing.Process(target = timer_pusher, args = (command_queue, time_before, command))
    timer.start()
    time_before = 12
    command = {'order': 'humpa',}
    timer = multiprocessing.Process(target = timer_pusher, args = (command_queue, time_before, command))
    timer.start()
    


def lumpator():
    """Summary
    """
    #Start the process which will attempt to quit the display

    command_queue = multiprocessing.Queue()  

    go_lumpa = True
    while go_lumpa is True:
        time_before = 5
        command = {'order': 'humpa',}
        timer = multiprocessing.Process(target = timer_pusher, args = (command_queue, time_before, command))
        timer.start()
        sleep(6)




#################################################
#     Main
#################################################
if __name__ == '__main__':
    lumpator()
