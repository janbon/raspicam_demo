# encoding: utf-8

"""This is the a camera example. It will save an image on desktop

.. moduleauthor:: Charles Fosseprez

.. contact:: charles.fosseprez@cri-paris.org

"""

###############################################################################
"""Import and settings"""
###############################################################################
###
# Import generics
from time import sleep
import os
###


###
# Import camera
fake_camera = False  # to tune the import
if fake_camera is False:
    import picamera
elif fake_camera is True:
    import magik_camera.picamera_fake as picamera
###


###
# Info
dirpath = os.getcwd()
print("current directory is : " + dirpath)
###



###############################################################################
"""Camera process"""
###############################################################################

###
# Catch camera
camera = picamera.PiCamera()   # get the camera

###
# Capture setting
camera.awb_gains = (1.8, 1.5)
camera.shutter_speed = 30000
camera.exposure_mode = 'off'

###
# Capture proper
sleep(2)  # Wait for it to initiialise
camera.capture('transcam.png', format='png')   # save image in current folder
