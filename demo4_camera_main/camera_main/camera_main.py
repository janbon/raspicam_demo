# encoding: utf-8

"""This is the a camera example. It will save an image on desktop

.. moduleauthor:: Charles Fosseprez

.. contact:: charles.fosseprez@cri-paris.org

"""

###############################################################################
"""Import and settings"""
###############################################################################
###
# Import generics
from time import sleep
import os
###


###
# Import camera
fake_camera = False  # to tune the import
if fake_camera is False:
    import picamera
elif fake_camera is True:
    import magik_camera.picamera_fake as picamera
###



###
# Should I display?
global showme
showme = True  # the showme parameters allow to tune the display of text
print("[i] display is set to: " + showme)


def checkit_(iamtext):
    global showme
    if showme is True:
        print(iamtext)
###


###
# Info
global dirpath
dirpath = 'mypath'  # here put your path, careful about the / or \ at the end. Should be a join.
checkit_("[i] current directory is : " + dirpath)

global imname
imname = 'im'  # here put your name, don't put extension. Png by definition, could be tiff.
checkit_("[i] current name is : " + imname)
###



###############################################################################
"""Camera process"""
###############################################################################
def capture():
    global dirpath
    ###
    # Catch camera
    camera = picamera.PiCamera()   # get the camera

    ###
    # Capture setting
    camera.awb_gains = (1.8, 1.5)
    camera.shutter_speed = 30000
    camera.exposure_mode = 'off'

    ###
    # Capture proper
    sleep(2)  # Wait for it to initiialise
    camera.capture(dirpath + imname + '.png', format='png')   # save image in current folder
