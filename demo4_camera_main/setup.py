from distutils.core import setup

setup(name='camera_main',
      version='0.1',
      description='change the path, install package, save to the path when called',
      url='',
      author='Charles D. Fosseprez',
      author_email='charles.fosseprez@cri-paris.org',
      license='MIT',
      package_dir = {
            'camera_main': 'camera_main',
                  },
      packages=['camera_main',
                  ]
      )


