###############################################################################
# import generics
###############################################################################
import unittest
import StringIO
import sys


###############################################################################
# Define test Case
###############################################################################


class Testimport_magik_camera(unittest.TestCase):


    def test_import_magik_camera(self):
        #growell_pack
        #   main package
        import magik_camera


    def test_magik_camera(self):        
        print('*********************')
        print('magik_camera')
        print('*********************')
        print('ask from testD_import')
        result = magik_camera.testD_import.showme()
        self.assertEqual(result, "I am imported")


class Testimport_camera_main(unittest.TestCase):


    def test_import_camera_main(self):
        #growell_pack
        #   main package
        import camera_main


    def test_growell_pack(self):        
        print('*********************')
        print('camera_main')
        print('*********************')
        print('ask  from testD_import')
        result = camera_main.testD_import.showme()
        self.assertEqual(result, "I am imported")
###############################################################################
# Do test
###############################################################################


if __name__ == '__main__':
    unittest.main()
