

import picamera_fake as camera_fake

import matplotlib.pyplot as plt


import os
 


def test_mini():
    print('some info: ')
    print camera_fake.__file__

    dirpath = os.getcwd()
    print("current directory is : " + dirpath)
    foldername = os.path.basename(dirpath)
    print("Directory name is : " + foldername)


    with camera_fake.PiCamera() as camera:
        global camera
        print('[>] Camera_fake test')
        im = camera.capture_mini()

    im.show()


def test_save():
    name = 'test'
    with camera_fake.PiCamera() as camera:
        global camera
        print('[>] Camera_fake test')
        camera.capture(name, format = 'png')


