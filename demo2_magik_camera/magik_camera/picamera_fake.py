
import sys
from PIL import Image, ImageDraw
import matplotlib.pyplot as plt

global image_fake_marked
image_fake = Image.new('RGB', (600, 300), color = 'black')
d = ImageDraw.Draw(image_fake)
d.text((270,140), "I am fake", fill=(255,255,255))
image_fake_marked = image_fake
print('[>] Camera is set to fake via testtools.config')


class PiCamera():

    awb_gains = 0

    framerate = 0

    shutter_speed = 0

    exposure_mode = 0

    iso = 0


    def __enter__(self):
        print('fake camera called')
        return self


    def __exit__(self, exc_type, exc_value, exc_traceback):
        print('fake camera closed')


    def capture_mini(self):
        print('[i] give fake image!')

        global image_fake_marked

        return image_fake_marked


    def capture(self, name, format):
        print('[i] save fake image! As: ' + str(name) + '.png')
        global image_fake_marked

        image_fake_marked.save(name + '.png')
