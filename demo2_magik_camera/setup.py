from distutils.core import setup

setup(name='magik_camera',
      version='0.1',
      description='contain tools for testing the camera',
      url='',
      author='Charles D. Fosseprez',
      author_email='charles.fosseprez@cri-paris.org',
      license='MIT',
      package_dir = {
            'magik_camera': 'magik_camera',
                  },
      packages=['magik_camera',
                  ]
      )


