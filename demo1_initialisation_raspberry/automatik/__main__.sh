#!/bin/bash
#setup the ssh by pushing the ssh keys
clear
run=1
while [[ "$run" = 1 ]]
do
  echo "Protocols:"
  echo "[i] share ssh first"
  echo "----------"  
  echo " 1 : push and execute"
  echo " 2 : reboot"
  echo " 5 : pull"  
  echo " 42 : quit"  
  echo " (autres): return"
  echo "----------" 
  read -p "choix: " rep
  if [  "$rep" = 1 ]
  then
      bash __main__executor.sh
  elif [  "$rep" = 2 ]
  then
      bash __main__rebootor.sh  
  elif [  "$rep" = 5 ]
  then
      git config credential.helper store
      git pull
  elif [  "$rep" = 42 ]
  then
      run=0
  else
      run=1
  fi
done
echo "by"
