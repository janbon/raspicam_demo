#!/bin/bash

#update the pull repo
git pull

#to_push_path="/home/temugin/to_push/"
ansible_role_path="/home/temugin/growell/ansible_hybrid/"
#ansible inventory
inventory="inventory.inv"

#playbook
training_cameraBook="playbook_install/training_raspberlingBook.yml"




#move to ansible roles dir
cd "$ansible_role_path"

# Some info https://stackoverflow.com/questions/21158689/ansible-deploy-on-multiple-hosts-in-the-same-time
#install to mother
ansible-playbook -bi "$inventory" "$training_motherBook" &

#install to raspberlig1
ansible-playbook -bi "$inventory"  "$training_raspberBook1" &

#install to raspberlig2
ansible-playbook -bi "$inventory"  "$training_raspberBook2" &

#install to raspberlig3
#ansible-playbook -bi "$inventory"  "$install_raspberBook3" &

#install to raspberlig4
#ansible-playbook -bi "$inventory"  "$install_raspberBook4" &

echo "done"
