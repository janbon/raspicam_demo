
What its all about
==================
this doc detail the procedure to prepare the raspberries..

OS settting
===========
This need to be done once. To install the OS on the machien and fix the IP.

1 Install Raspbian OS
---------------------
Get raspbian image. https://www.raspberrypi.org/downloads/raspbian/
Flash the SD card with rapsbian. https://www.raspberrypi.org/documentation/installation/installing-images/

2 Fix IP adress
---------------
You need to fixe the IP adress of the Raspberries.
In order to do so. Follow these steps.
Open a terminal and type.
```bash
sudo nano /etc/dhcpcd.conf
```

Scroll all the way to the bottom of the file and add one, or both of the following snippets. Depending on whether you want to set a static IP address for a wired connection or a wireless connection eth0 = wired, wlan0 = wireless.
You’ll need to edit the numbers in the snippet so they match your network configuration.
:warning: You need to manually report the IP adress fixed into the python config and the ansible inventory.

```
interface eth0

static ip_address=192.168.0.10/24
static routers=192.168.0.1
static domain_name_servers=192.168.0.1

interface wlan0

static ip_address=192.168.0.200/24
static routers=192.168.0.1
static domain_name_servers=192.168.0.1
```

interface = This defines which network interface you are setting the configuration for.
static ip_address = This is the IP address that you want to set your device to. (Make sure you leave the /24 at the end)
static routers = This is the IP address of your gateway (probably the IP address or your router)
static domain_name_servers = This is the IP address of your DNS (probably the IP address of your router). You can add multiple IP addresses here separated with a single space.

 

To exit the editor, press ctrl+x
To save your changes press the letter “Y” then hit enter



Now all you need to do is reboot, and everything should be set!
```bash
reboot
```
You can double check by typing
```bash
ifconfig
```
And checking the interfaces IP address


SSH settting
============
This need to be done once ideally. But it is possible that this step need to be repeated before use.
The .sh scripts are located in the ansible_hybrid/ folder.

3 Share SSH keys
----------------
Once this is done, you don't need to access the raspberries anymore, they are accessible over network and from your computer.
Use the script auto_ssh_push.sh on your computer to share SSH keys with the receiver machines.
This need to be done for every machines.

Machine installer
=================
This need to be done once. But if some news modules were added, they need to figure in this file.
The .sh scripts are located in the ansible_hybrid/ folder.

4 Install the machines
----------------------

Use main_install_script.sh 
This will use ansible to install the basic components on your Raspberries (usb read/write mode, supress screensaver.. python modules..)


Machine updater
===============
This is done each time you want to push a new code on the machines.
The .sh scripts are located in the ansible_hybrid/ folder.

5 Pull the git on your machine
------------------------------

Use the script git_pull to download the up to date version of the codes.

6 Ansible the code on the receiver machines
-------------------------------------------

Use main_update_script.sh to push the new code to your machines.

5+6 Combinaison of the two previous steps
-----------------------------------------
Use the main_update_script_gitpull_.sh