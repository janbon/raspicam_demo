

What it's all about
===================

This folder detail the raspicamera. And related code.
It will speak about the basic setup of the camera. The use of the camera in a python script.  
![Alt text](images/RaspiCamV2.png?raw=true "The beast")  

Some example function.
And test help.
But also detail some more complex montage of process and scripts.

Complete use case include the utilisation of ansible in order to configure the raspicam.


Want to know more?
==================

Very interesting material is available online.  

For example some basic material:  
https://picamera.readthedocs.io/en/release-1.13/  
https://projects.raspberrypi.org/en/projects/getting-started-with-picamera

Fast manipulation with OpenCV:  
https://www.uco.es/investiga/grupos/ava/node/40  

Advanced applications example:  
https://publiclab.org/wiki/near-infrared-camera  

About Git
=========

Git is a versionning tool. That allow to keep track of the code over its progress.
But also to download it very easely from a remote server were the code was push.
Here we use bitbucket as a remote repo.

For example, into a terminal on your raspberry typethe following.
```
git clone https://janbon@bitbucket.org/janbon/raspicam_demo.git
```
Content
=======

1. Initialise Raspberry and deploy your scripts.
	This detail the procedure to follow in order to install Raspbian, and activate the camera onto your Raspberry

2. Magik_camera
	This Little package allow to develop script working with the Picamera, without Picamera. Look at "Mock" and "Mocking package".  
	In order to install type in a terminal (rooted in the package folder were the setup.py file is located):  
	```
	python setup.py install
	```

3. Use the Picamera module
	Some scripts in order to use the camera.


4. Camera_main 
	This Little package allow to capture an image to a given path.   
	In order to install type in a terminal (rooted in the package folder were the setup.py file is located):  
	```
	python setup.py install
	```


5. Tests
	This aim to detail some testing.


42. Extra
	Some arduino stuff.
	Some rabbitMQ stuffs.



Details on Raspberry and Raspbian
=================================

Raspbian
--------
The raspbery run Raspbian as an OS.
Interfacing options need to be checked in order to open the camera port.
Raspbian is a branch of unix, its ARM processor architecture offer some limitations in term of librairy instalation (for example python pip may take eternity, building from .whl is faster but be careful about your versions).

Hardware
--------
Is weak.
https://picamera.readthedocs.io/en/release-1.13/fov.html
But sufficient.


Python
------
We highly recommend to shift to Python 3.
Careful about the environement and your python path.
The folder Python_courses is about some proffesional Python formation.
If you need info, remenber that the help() function work well.  
:volcano: Python is slow. Know your weakness, do multiprocess!
